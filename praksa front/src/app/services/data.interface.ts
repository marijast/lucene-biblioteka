import { Observable } from 'rxjs';
import { User } from '../models/user.model';


export interface Data {
   

    getUserById(id: number): Observable<User>;
    register(user: User): Observable<any>;
    getAllUsers(): Observable<User[]>;  
}