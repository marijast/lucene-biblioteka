import { Data } from './data.interface';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user.model';
import { Post } from '../models/post.model';
import { Firma } from '../models/firma.model';
import { Pretraga } from '../models/pretraga.model';
import { NoviUpit } from '../models/noviUpit.model';

@Injectable(
    {providedIn: "root"}
)
export class DataService implements Data {
  
    
    private baseUrl: string = "https://localhost:5001/api/";
    sviOglasi: Post[];
    vratiIdFirme:number;
   

    constructor(private http: HttpClient) {

    }
    
    private convertUserToPayload(user: User): any {
        let payload: any = {
            Ime: user.ime,
            Prezime: user.prezime,
            Username: user.username,
            Password:user.password,
            Email:user.email
        }

        return payload;
    }
    getUserById(id: number): Observable<User> {
        let url: string = this.baseUrl + "user/" + id;
        return this.http.get<User>(url);
    }
    register(user: User): Observable<any>{
        let url: string = this.baseUrl + "user/registrujSe";
        let payload: any = this.convertUserToPayload(user);
        return this.http.post(url, payload);
    }
    firmaCijiJePost(idPosta: number): Observable<Firma> {
        let url: string = this.baseUrl + "firma/ucitaj/" + idPosta;
        return this.http.get<Firma>(url);
    }
    login(username: string, password: string): Observable<any> {
        let url: string = this.baseUrl + "user/LogIn";
        let payload: any = {
            username: username,
            password: password
        };
        return this.http.post<any>(url, payload, {responseType: "json"});
    }
    getUserByUsername(username: string): Observable<User>{
        let url: string = this.baseUrl + "user/byUsername/" + username;
        return this.http.get<User>(url);
    }
    getPostById(id: number): Observable<Post> {
        let url: string = this.baseUrl + "post/ucitaj/" + id;
        return this.http.get<Post>(url);
    }
    Firma(): boolean {
        let admin: string = localStorage.getItem("firma");
        return admin != null;
    }
    User(): boolean {
        let userLogged: string = localStorage.getItem("user");
        return userLogged != null;
    }
    isUserLogged(): boolean {
        let username: string = localStorage.getItem("username");
        return username != null;
    }

    isUser(): boolean {
        let username: string = localStorage.getItem("ulogovanPraktikant");
        return username != null;
    }

    addPost(post: Post): Observable<any>{
        let url: string = this.baseUrl + "post/addPost";
        let payload: any = {
            Naziv: post.naziv,
            Opis: post.opis,
            Trajanje: post.trajanje,
            Grad: post.grad,
            Firma:post.firma
        }
        return this.http.post(url, payload);
    }

    firmaByUsername(username:string):Observable<any> {
        let url: string = this.baseUrl + "firma/getFirmaByUsername/" + username ;
       return this.http.get<Firma>(url);
     }

    addFirma(firma: Firma): Observable<any>{
        let url: string = this.baseUrl + "firma/addFirma";
        let payload: any = {
            Naziv: firma.naziv,
            Username: firma.username,
            Password: firma.password,
            Adresa: firma.adresa,
            Logo: firma.logo,
            Link: firma.link,
            Email: firma.email,
        }
        return this.http.post(url, payload);
    }
    getAllUsers(): Observable<User[]> {
        let url: string = this.baseUrl + "user/" ;
        return this.http.get<User[]>(url);
    }
    getAllPosts(): Observable<Post[]> {
        let url: string = this.baseUrl + "post/" ;
        return this.http.get<Post[]>(url);
    }
    getFirme(): Observable<Firma[]> {
        let url: string = this.baseUrl + "firma/" ;
        return this.http.get<Firma[]>(url);
    }

    sviOglasiFirme(firma: Firma): void {
        let url: string = this.baseUrl + "firma/vratiSveOglase";
        this.http.post<Post[]>(url,firma).subscribe(
            (data: Post[]) => {
                this.sviOglasi = data;
            }
        );
    }
    idFirme(firma: Firma): void {
        let url: string = this.baseUrl + "firma/vratiId";
        this.http.post<number>(url,firma).subscribe(
            (data: number) => {
                this.vratiIdFirme = data;
            }
        );
    }
    oglasiFirme(username:string) :Observable<any> {
        let url: string = this.baseUrl + "firma/oglasiFirme/" + username ;
       return this.http.get<Post[]>(url);
     }
     pretraga(upit: string): Observable<any>{
        let url: string = this.baseUrl + "post/pretraga/" + upit;
        return this.http.get<Post[]>(url);
    }
    naprednaPretraga(query: string): Observable<any>{
        let url: string = this.baseUrl + "post/naprednaPretraga/" + query;
        return this.http.get<Post[]>(url);
    }
    naprednaPretraga1(parametri: Pretraga): Observable<Post[]>{
        let url: string = this.baseUrl + "post/naprednaPretraga1";
        return this.http.post<Post[]>(url, parametri);
    }
    zaObavestenje(parametri: NoviUpit): Observable<any>{
        let url: string = this.baseUrl + "user/zaObavestenje";
        return this.http.post<any>(url, parametri);
    }
   
    pretraziGrad(grad: string): Observable<any>{
        let url: string = this.baseUrl + "post/pretragaGrad/" + grad;
        return this.http.get<Post[]>(url);
    }

    obrisiPost(id:Number):Observable<any> {
        let url: string = this.baseUrl + "post/" + id;
            return this.http.delete(url);
        }
    apply(username:string, password:string,id:number) :Observable<any>{
        let url: string = this.baseUrl + "user/prijavaZaPraksu";
        let klijent = {
            Username: username,
            Password: password
        };
        let payload: any = {
            Username: username,
            Password:password,
            IdPosta:id,
        };
        return this.http.post<any>(url, payload);
    }
    
}