import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddPostComponent } from './components/add-post/add-post.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { RegistracijaComponent } from './components/registracija/registracija.component';
import { OglasiJedneFirmeComponent } from './components/oglasi-jedne-firme/oglasi-jedne-firme.component';
import { FirmaComponent } from './components/firma/firma.component';
import { SviPostoviComponent } from './components/svi-postovi/svi-postovi.component';
import { KompanijeComponent } from './components/kompanije/kompanije.component';
import { Home1Component } from './components/home1/home1.component';
import { LogovanjeZaPrijavuComponent } from './components/logovanje-za-prijavu/logovanje-za-prijavu.component';
import { ObavestenjaComponent } from './components/obavestenja/obavestenja.component';
import { PostComponent } from './components/post/post.component';

const routes: Routes = [

  {path: 'nl', component: LogovanjeZaPrijavuComponent},
  {path: 'addpost', component: AddPostComponent},
  {path: 'home', component: Home1Component},
  {path: 'oglasi', component: HomeComponent},
  {path: 'kompanije', component: KompanijeComponent},
  {path: 'post/:id', component: PostComponent},
  {path: 'ob', component: ObavestenjaComponent},
  {path: 'oglasifirme', component: OglasiJedneFirmeComponent},
  {path: '', component: Home1Component},
  {path: '**', component: Home1Component},
  


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
