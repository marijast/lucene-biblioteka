import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KompanijeComponent } from './kompanije.component';

describe('KompanijeComponent', () => {
  let component: KompanijeComponent;
  let fixture: ComponentFixture<KompanijeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KompanijeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KompanijeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
