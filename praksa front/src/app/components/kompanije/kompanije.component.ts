import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Firma } from 'src/app/models/firma.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-kompanije',
  templateUrl: './kompanije.component.html',
  styleUrls: ['./kompanije.component.scss']
})
export class KompanijeComponent implements OnInit {
firme :Firma[];
myBackgroundImageUrl = 'assets/m.jpg';

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.ucitajFirme();
  }
  ucitajFirme(): void {
    this.dataService.getFirme().subscribe(
      (data: Firma[]) => {
        this.firme = data;
      }
    );
    }
    getBackgroundImageUrl() {
      return `url(${this.myBackgroundImageUrl})`
    }
}
