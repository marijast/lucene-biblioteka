import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/user.model';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Firma } from 'src/app/models/firma.model';
import { FirmaComponent } from '../firma/firma.component';
import { RegistracijaComponent } from '../registracija/registracija.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username: string;
  password: string;
  firma:Firma;
  id:string;
  user: User; 
  @ViewChild('form',{static: false})
  form: NgForm;
  

  constructor(private dataService: DataService, private router: Router,
    private snackBar: MatSnackBar,public dialog: MatDialog) {
    
   }

  ngOnInit() {
    
  }
  login(): void {
    let username: string = this.username;
    let password: string = this.password;
    this.dataService.login(username,password).subscribe(
      (type: any) => {
       let userAccountType = type.tip;
       this.navigateToAdminPanel(username, password, userAccountType);

       if(userAccountType == "Firma") {
         return;
       }

        this.dataService.getUserByUsername(username).subscribe(
          (data: any) => {
           //  localStorage.setItem("ulogovanPraktikant", "ulogovanPraktikant");
            localStorage.setItem("username", data.username);
            localStorage.setItem("password", data.password);
            this.router.navigate(["oglasi"]);
            let user: User = new User();
            user.username = username;
            user.password = password;
  
    
        //    this.dataService.getUserReservations(klijent);
          },
        
        );
      },
      (error) => {
        //neuspesan login
       console.log(error);
       let message = "Neuspešna prijava.Pogresili ste lozinku ili korisnicko ime";
       this.snackBar.open(message, "Zatvori", {
         duration: 2000,
       });
     }
     
    );
 }
 navigateToAdminPanel(username: string, password: string, accountType: string): void {

  if(accountType != "Firma"){
    return;
  }
  localStorage.setItem("firma", "firma");
  localStorage.setItem("username", username);
  localStorage.setItem("password", password);
  let novaFirma: Firma = new Firma();
  novaFirma.username = username;
  novaFirma.password = password;
   /* this.dataService.firmaByUsername(username).subscribe(
      (data: Firma) => {
        this.firma=data;
      }
    );
    
  localStorage.setItem("naziv", this.firma.naziv);
  localStorage.setItem("adresa", this.firma.adresa);
  localStorage.setItem("logo", this.firma.logo);
  localStorage.setItem("link", this.firma.link);
  localStorage.setItem("email", this.firma.email);*/
    
  //this.dataService.sviOglasiFirme(novaFirma);
  
  this.router.navigate(["home"]);

}
openDialogRegUser(): void {
  const dialogRef = this.dialog.open(RegistracijaComponent, {
    width: '500px',
    height: '600px'
  });
}
openDialogRegCompany(): void {
  const dialogRef = this.dialog.open( FirmaComponent
  , {
    width: '500px',
    height: '600px'
  });
}

}


/*  onSubmit() {
    this.dataService.login(this.username,this.password).subscribe(
      (success: boolean) => {
        if(!success){
          return;
          //ovde ispisati da je neuspesna registracija
        }
     //   this.router.navigate(['login']);
      }
    );
    this.form.reset();
  }
  
 /* loadUsers(): void {
    this.dataService.getAllUsers().subscribe(
      (data: User[]) => {
        this.users = data;
      }
    );
  }*/





