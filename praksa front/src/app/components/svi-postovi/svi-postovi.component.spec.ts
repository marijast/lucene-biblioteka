import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SviPostoviComponent } from './svi-postovi.component';

describe('SviPostoviComponent', () => {
  let component: SviPostoviComponent;
  let fixture: ComponentFixture<SviPostoviComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SviPostoviComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SviPostoviComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
