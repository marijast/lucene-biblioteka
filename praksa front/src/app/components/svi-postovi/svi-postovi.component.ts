import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-svi-postovi',
  templateUrl: './svi-postovi.component.html',
  styleUrls: ['./svi-postovi.component.scss']
})
export class SviPostoviComponent implements OnInit {
  posts: Post[];

  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
    this.loadPosts();
  }

  loadPosts(): void {
    this.dataService.getAllPosts().subscribe(
      (data: Post[]) => {
        this.posts = data;
      }
    );
    }
}
