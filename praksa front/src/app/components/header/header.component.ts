import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import {  ActivatedRoute, ParamMap } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { RegistracijaComponent } from '../registracija/registracija.component';
import { FirmaComponent } from '../firma/firma.component';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isAdmin: boolean;

  constructor(private dataService: DataService, private router: Router,public dialog: MatDialog) { }

  ngOnInit() {
  }
  isLogged(): boolean {
    return this.dataService.isUserLogged();
  }

  logOut(): void {
    localStorage.removeItem("username");
    localStorage.removeItem("password");
    localStorage.removeItem("firma");
    //localStorage.removeItem("ulogovanPraktikant");
//localStorage.removeItem("rezervisano");
  //  this.dataService.rezervacijaKorisnika = null;
    this.router.navigate(['home']);

  }

  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      height: '500px'
    });
  }
  openDialogRegUser(): void {
    const dialogRef = this.dialog.open(RegistracijaComponent, {
      width: '500px',
      height: '600px'
    });
  }

  openDialogRegCompany(): void {
    const dialogRef = this.dialog.open( FirmaComponent
    , {
      width: '500px',
      height: '600px'
    });
  }
}
