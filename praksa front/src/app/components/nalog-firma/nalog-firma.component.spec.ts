import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NalogFirmaComponent } from './nalog-firma.component';

describe('NalogFirmaComponent', () => {
  let component: NalogFirmaComponent;
  let fixture: ComponentFixture<NalogFirmaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NalogFirmaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NalogFirmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
