import { ViewChild } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Firma } from 'src/app/models/firma.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-nalog-firma',
  templateUrl: './nalog-firma.component.html',
  styleUrls: ['./nalog-firma.component.scss']
})
export class NalogFirmaComponent implements OnInit {

  firma: Firma; 
  @ViewChild('form',{static: false})
  form: NgForm;
  constructor(private dataService: DataService) { 
    this.firma = new Firma();

  }

  ngOnInit() {
  }

  onSubmit() {
    this.dataService.addFirma(this.firma).subscribe(
      (success: boolean) => {
        if(!success){
          return;
          //ovde ispisati da je neuspesna registracija
        }
     //   this.router.navigate(['login']);
      }
    );
    this.form.reset();
  }

}
