import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatSnackBar } from '@angular/material';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Firma } from 'src/app/models/firma.model';
import { Post } from 'src/app/models/post.model';
import { DataService } from 'src/app/services/data.service';
import { LogovanjeZaPrijavuComponent } from '../logovanje-za-prijavu/logovanje-za-prijavu.component';


@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss']
})
export class PostComponent implements OnInit {
  @Input()
  set post(value: Post) {
    this._post = value;
   // this.loadFirma();
  }

  get post() {
    return this._post;
  }
  _post: Post; 
  firma: Firma;
  username:string;
  password:string;
  constructor(private dataService: DataService, private router: Router,private route: ActivatedRoute,
     private snackBar: MatSnackBar,public dialog: MatDialog) { }
     myBackgroundImageUrl = 'assets/sss.jpg';
  ngOnInit() {

 this.loadPost();
  }
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }

loadPost(): void {
    this.route.params.subscribe(
      (params: Params) => {
        let id: number = params["id"];  
        this.dataService.getPostById(id).subscribe(
          (post: Post) => {
            this.post = post;
          }
        );
      }
    );
  }
  /*loadFirma(): void {
    this.dataService.firmaCijiJePost(this._post.id).subscribe(
      (data: Firma) => {
        this.firma = data;
      }
    );
  }*/

  apply():void{
    this.username = localStorage.getItem("username");
    this.password = localStorage.getItem("password");
    this.dataService.apply(this.username, this.password,this._post.id).subscribe(
      (data:any) =>
      {
        this.ngOnInit();
        {
          let odgovor = data.tip;
       if(odgovor == "Uspesna prijava") {
         let message = "Uspesna prijava";
         this.snackBar.open(message, "Zatvori", {
           duration: 2000,
         });
       }
       else
       {
     /*   let message = "Morate se ulogovati da biste se prijavili";
        this.snackBar.open(message, "Zatvori", {
          duration: 5000,
        });*/
        this.openLoginForm();
       }
      }
    }
    );
    
    (error) => {
      //neuspesan login
     console.log(error);
     let message = "Neuspesna prijava";
     this.snackBar.open(message, "Zatvori", {
       duration: 5000,
     });
   }
   
  }  
  
  openLoginForm():void
  {
    const dialogRef = this.dialog.open( LogovanjeZaPrijavuComponent
      , {
        width: '500px',
        height: '210px'
      });
  }
}
