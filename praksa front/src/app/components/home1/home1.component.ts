import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { FirmaComponent } from '../firma/firma.component';
import { LoginComponent } from '../login/login.component';
import { RegistracijaComponent } from '../registracija/registracija.component';

@Component({
  selector: 'app-home1',
  templateUrl: './home1.component.html',
  styleUrls: ['./home1.component.scss']
})
export class Home1Component implements OnInit {
  myBackgroundImageUrl = 'assets/aa.jpg';
  myBackgroundImageUrl1 = 'assets/a1.jpg';
  myBackgroundImageUrl2 = 'assets/s6.jpg';
  myBackgroundImageUrl3 = 'assets/s4.jpg';

  constructor(private dataService: DataService, private router: Router,public dialog: MatDialog) { }

  ngOnInit() {
  }
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
  getBackgroundImageUrl1() {
    return `url(${this.myBackgroundImageUrl1})`
  }
  getBackgroundImageUrl2() {
    return `url(${this.myBackgroundImageUrl2})`
  }
  getBackgroundImageUrl3() {
    return `url(${this.myBackgroundImageUrl3})`
  }
  isLogged(): boolean {
    return this.dataService.isUserLogged();
  }

  logOut(): void {
    localStorage.removeItem("username");
    localStorage.removeItem("password");
    localStorage.removeItem("firma");
    this.router.navigate(['home']);

  }
  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      height: '500px'
    });
  }
  openDialogRegUser(): void {
    const dialogRef = this.dialog.open(RegistracijaComponent, {
      width: '500px',
      height: '600px'
    });
  }
  openDialogRegCompany(): void {
    const dialogRef = this.dialog.open( FirmaComponent
    , {
      width: '500px',
      height: '600px'
    });
  }
}
