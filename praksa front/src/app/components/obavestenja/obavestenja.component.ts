import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-obavestenja',
  templateUrl: './obavestenja.component.html',
  styleUrls: ['./obavestenja.component.scss']
})
export class ObavestenjaComponent implements OnInit {
 mail:string;
  constructor(private dialogRef: MatDialogRef<ObavestenjaComponent>) { 
   
  }

  ngOnInit() {
  }

  zatvori() {
    this.dialogRef.close('');
  }
  prijavaZaObavestenje()
  {
    this.dialogRef.close(this.mail);

  }
}
