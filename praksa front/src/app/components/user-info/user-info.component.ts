import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';

@Component({
  selector: 'app-user-info',
  templateUrl: './user-info.component.html',
  styleUrls: ['./user-info.component.scss']
})
export class UserInfoComponent implements OnInit {
 
  @Input()
  private user: User;
  constructor(private router: Router) { }

  ngOnInit() {
  }
  loadUser(): void {
    this.router.navigate(['user', this.user.id]);
  }
}
