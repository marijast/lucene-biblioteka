import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JednaFirmaComponent } from './jedna-firma.component';

describe('JednaFirmaComponent', () => {
  let component: JednaFirmaComponent;
  let fixture: ComponentFixture<JednaFirmaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JednaFirmaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JednaFirmaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
