import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Firma } from 'src/app/models/firma.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-jedna-firma',
  templateUrl: './jedna-firma.component.html',
  styleUrls: ['./jedna-firma.component.scss']
})
export class JednaFirmaComponent implements OnInit {
  @Input()
  private firma: Firma; 
  constructor(private dataService: DataService, private router: Router) { }

  ngOnInit() {
  }

}
