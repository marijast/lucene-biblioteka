import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Obavestenja1Component } from './obavestenja1.component';

describe('Obavestenja1Component', () => {
  let component: Obavestenja1Component;
  let fixture: ComponentFixture<Obavestenja1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Obavestenja1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Obavestenja1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
