import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-obavestenja1',
  templateUrl: './obavestenja1.component.html',
  styleUrls: ['./obavestenja1.component.scss']
})
export class Obavestenja1Component implements OnInit {
  mail:string;

  constructor(private dialogRef1: MatDialogRef<Obavestenja1Component>) { }

  ngOnInit() {
  }
  zatvori1() {
    this.dialogRef1.close('');
  }
  prijavaZaObavestenje1()
  {
    this.dialogRef1.close(this.mail);

  }
}
