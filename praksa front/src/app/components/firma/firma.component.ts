import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { Firma } from 'src/app/models/firma.model';
import { DataService } from 'src/app/services/data.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-firma',
  templateUrl: './firma.component.html',
  styleUrls: ['./firma.component.scss']
})
export class FirmaComponent implements OnInit {
  firma: Firma; 
  @ViewChild('form',{static: false})
  form: NgForm;
  
  constructor(private dataService: DataService, private router: Router,private snackBar: MatSnackBar,public dialog: MatDialog) { }

  ngOnInit() {
  this.firma=new Firma();
  }

  onSubmit() {
    this.dataService.addFirma(this.firma).subscribe(
      (type: any) => {
       let odgovor = type.tip;
       if(odgovor == "Postoji") {
        let message = "Korisnicko ime koje ste uneli vec postoji";
        this.snackBar.open(message, "Zatvori", {
          duration: 2500,});

       }
       else{
        let message = "Uspesna registracija";
        this.snackBar.open(message, "Zatvori", {
          duration: 2500,});
     // this.router.navigate(['login']);
     this.openDialogLogin();
       }
      }
    );
    this.form.reset();
  }
  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      height: '500px'
    });
  }

}
