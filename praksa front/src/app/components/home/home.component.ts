import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { NgForm } from '@angular/forms';
import { Pretraga } from 'src/app/models/pretraga.model';
import { MatDialog, MatDialogRef } from '@angular/material';
import { ObavestenjaComponent } from '../obavestenja/obavestenja.component';
import { NoviUpit } from 'src/app/models/noviUpit.model';
import { Obavestenja1Component } from '../obavestenja1/obavestenja1.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  post: Post;
  posts: Post[];
  query: string;
  queryM: string;
  gradNP: string;
  trajanje: string;
  grad: string;
  @ViewChild('form', { static: false })
  form: NgForm;
  myBackgroundImageUrl = 'assets/ma.jpg';
  savedQuery: string;
  savedQueryM:string;
  dialogRef: MatDialogRef<ObavestenjaComponent>;
  dialogRef1: MatDialogRef<Obavestenja1Component>;

  constructor(private dataService: DataService, private router: Router, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.loadPosts();

  }

  loadPosts(): void {
    this.dataService.getAllPosts().subscribe(
      (data: Post[]) => {
        this.posts = data;
      }
    );
  }
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`;
  }
  pretrazi() {
    this.savedQuery = this.query; // savedQuery  koristiti za obavestenja
    this.dataService.pretraga(this.query).subscribe(
      (data: Post[]) => {
        if (data && data.length !== 0) {
          this.posts = data;
        }
        else {
          this.openDialogNotify();
        }

      }
    );
    this.form.reset();
  }

  pretraziGrad() {

    this.dataService.pretraziGrad(this.grad).subscribe(
      (data: Post[]) => {
        this.posts = data;

      }
    );
    this.form.reset();
  }
  naprednaPretraga() {
    this.queryM = this.gradNP + " " + this.trajanje;
    this.savedQuery= this.queryM; // savedQuery  koristiti za obavestenja
    this.savedQueryM = this.queryM; // savedQuery  koristiti za obavestenja

    this.dataService.naprednaPretraga(this.queryM).subscribe(
      (data: Post[]) => {
        if (data && data.length !== 0) {
          this.posts = data;
        }
        else {
          this.openDialogNotify();
        }

      }
    );
    this.form.reset();
  }
  naprednaPretraga1() {
    let parametri: Pretraga =
    {
      grad: this.gradNP,
      trajanje: this.trajanje
    }
    this.dataService.naprednaPretraga1(parametri).subscribe(
      (data: Post[]) => {
        this.posts = data;

      }
    );
    this.form.reset();
  }
  onSelectedChange(value: string) {
    // do something else with the value
    console.log(value);

    // remember to update the selectedValue
    this.trajanje = value;
  }

  openDialogNotify(): void {
    const dialogRef = this.dialog.open(ObavestenjaComponent
      , {
        width: '420px',
        height: '300px'
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(result)
      console.log(this.savedQuery)
      if(result!=='')
      {
        let parametri: NoviUpit =
        {
      mail: result,
      zadatiUpit: this.savedQuery
        }
    this.dataService.zaObavestenje(parametri).subscribe(
      (data: Post[]) => {
        this.posts = [];

      }
    );
      }
      else
      {
        this.posts=[];
      }
    });
  }
  openDialogNotify1(): void {
    const dialogRef1 = this.dialog.open(Obavestenja1Component
      , {
        width: '420px',
        height: '300px'
      });

    dialogRef1.afterClosed().subscribe(result => {
      if(result!=='')
      {
        let parametri: NoviUpit =
        {
      mail: result,
      zadatiUpit: this.savedQueryM
        }
    this.dataService.zaObavestenje(parametri).subscribe(
      (data: Post[]) => {
        this.posts = [];

      }
    );
      }
      else
      {
        this.posts=[];
      }
    });
  }
}
