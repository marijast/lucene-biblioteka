import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostPrikazComponent } from './post-prikaz.component';

describe('PostPrikazComponent', () => {
  let component: PostPrikazComponent;
  let fixture: ComponentFixture<PostPrikazComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostPrikazComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostPrikazComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
