import { Component, Input, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-post-prikaz',
  templateUrl: './post-prikaz.component.html',
  styleUrls: ['./post-prikaz.component.scss']
})
export class PostPrikazComponent implements OnInit {
  @Input()

  post: Post; 
  constructor(private dataService: DataService, private router: Router,private route: ActivatedRoute) { }

  ngOnInit() {
  }

  pogledajPost(): void {
    this.router.navigate(['post', this.post.id]);
  }
}
