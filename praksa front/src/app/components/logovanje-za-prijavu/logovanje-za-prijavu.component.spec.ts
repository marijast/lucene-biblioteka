import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogovanjeZaPrijavuComponent } from './logovanje-za-prijavu.component';

describe('LogovanjeZaPrijavuComponent', () => {
  let component: LogovanjeZaPrijavuComponent;
  let fixture: ComponentFixture<LogovanjeZaPrijavuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogovanjeZaPrijavuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogovanjeZaPrijavuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
