import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { RegistracijaComponent } from '../registracija/registracija.component';

@Component({
  selector: 'app-logovanje-za-prijavu',
  templateUrl: './logovanje-za-prijavu.component.html',
  styleUrls: ['./logovanje-za-prijavu.component.scss']
})
export class LogovanjeZaPrijavuComponent implements OnInit {

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }
  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      height: '500px'
    });
  }
  openDialogRegister(): void {
    const dialogRef = this.dialog.open(RegistracijaComponent, {
      width: '500px',
      height: '600px'
    });
  }

}
