import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Post } from 'src/app/models/post.model';
import { DataService } from 'src/app/services/data.service';
import { Router } from '@angular/router';
import { Firma } from 'src/app/models/firma.model';
import { getLocaleDateTimeFormat } from '@angular/common';

@Component({
  selector: 'app-add-post',
  templateUrl: './add-post.component.html',
  styleUrls: ['./add-post.component.scss']
})
export class AddPostComponent implements OnInit {
  post: Post; 
  posts: Post[];
  @ViewChild('form',{static: false})
  form: NgForm;
  firma: Firma;
  firmaUsername:string;
  myBackgroundImageUrl = 'assets/m.jpg';
  

  constructor(private dataService: DataService, private router: Router) {
    
   }

  ngOnInit() {
    this.post = new Post();
    this.firmaUsername=localStorage.getItem("username");
    this.findFirma();
  }
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
  findFirma()
  {
    this.dataService.firmaByUsername(this.firmaUsername).subscribe(
      (data: Firma) => {
        this.post.firma=data;

      }
    );
    } 
  onSubmit() {
    
    this.dataService.addPost(this.post).subscribe(
      (success: boolean) => {
        if(!success){
          return;
          
        }
     //   this.router.navigate(['login']);
      }
    );
    this.form.reset();
  }
  onSelectedChange(value: string) {
    // do something else with the value
    console.log(value);

    // remember to update the selectedValue
    this.post.trajanje = value;
  }
  

  }

