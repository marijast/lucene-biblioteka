import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BrisanjePostaComponent } from './brisanje-posta.component';

describe('BrisanjePostaComponent', () => {
  let component: BrisanjePostaComponent;
  let fixture: ComponentFixture<BrisanjePostaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BrisanjePostaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BrisanjePostaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
