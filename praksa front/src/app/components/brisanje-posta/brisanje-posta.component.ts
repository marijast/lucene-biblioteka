import { EventEmitter, Output } from '@angular/core';
import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Post } from 'src/app/models/post.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-brisanje-posta',
  templateUrl: './brisanje-posta.component.html',
  styleUrls: ['./brisanje-posta.component.scss']
})
export class BrisanjePostaComponent implements OnInit {
  @Input()
  private post: Post;
  @Output()
  notifyDelete: EventEmitter<any> = new EventEmitter<any>();
  constructor(private router: Router,private dataService: DataService) { }

  ngOnInit() {
  }

  obrisiPost(){
    this.dataService.obrisiPost(this.post.id).subscribe(
      () => {
        this.notifyDelete.emit();
        this.router.navigate(['oglasifirme']);
      }
    );
    

  }
}
