import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Firma } from 'src/app/models/firma.model';
import { Post } from 'src/app/models/post.model';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-oglasi-jedne-firme',
  templateUrl: './oglasi-jedne-firme.component.html',
  styleUrls: ['./oglasi-jedne-firme.component.scss']
})
export class OglasiJedneFirmeComponent implements OnInit {

  posts:Post[];
  post:Post;
  firma:Firma;
  username:string;
  

  
  constructor(private dataService: DataService,private router: Router) { }
  myBackgroundImageUrl = 'assets/m.jpg';

  ngOnInit() {
    this.username = localStorage.getItem("username");
    this.vratiOglase();
   
  }
  getBackgroundImageUrl() {
    return `url(${this.myBackgroundImageUrl})`
  }
  vratiOglase():void
  {
          this.dataService.oglasiFirme(this.username).subscribe(
            (data: Post[]) => {
              this.posts = data;
             
            }
          );
        }
        reload(): void {
          this.vratiOglase();
        }
}

