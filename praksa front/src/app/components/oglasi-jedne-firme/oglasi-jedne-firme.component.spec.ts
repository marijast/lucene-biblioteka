import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OglasiJedneFirmeComponent } from './oglasi-jedne-firme.component';

describe('OglasiJedneFirmeComponent', () => {
  let component: OglasiJedneFirmeComponent;
  let fixture: ComponentFixture<OglasiJedneFirmeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OglasiJedneFirmeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OglasiJedneFirmeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
