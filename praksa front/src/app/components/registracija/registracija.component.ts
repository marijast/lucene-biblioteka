import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { MatDialog, MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user.model';
import { DataService } from 'src/app/services/data.service';
import { LoginComponent } from '../login/login.component';

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.scss']
})
export class RegistracijaComponent implements OnInit {
  user: User; 
  @ViewChild('form',{static: false})
  form: NgForm;
  
  constructor(private dataService: DataService, private router: Router,private snackBar: MatSnackBar,public dialog: MatDialog) {
    this.user = new User();
   }

  ngOnInit() {
  }

  onSubmit() {
    this.dataService.register(this.user).subscribe(
      (type: any) => {
       let odgovor = type.tip;
       if(odgovor == "Postoji") {
        let message = "Korisnicko ime koje ste uneli vec postoji";
        this.snackBar.open(message, "Zatvori", {
          duration: 2500,});

       }
       else{
        let message = "Uspesna registracija";
        this.snackBar.open(message, "Zatvori", {
          duration: 2500,});
    //  this.router.navigate(['login']);
    this.openDialogLogin();
       }
      }
    );
    this.form.reset();
  }

  openDialogLogin(): void {
    const dialogRef = this.dialog.open(LoginComponent, {
      width: '400px',
      height: '500px'
    });
  }
}
