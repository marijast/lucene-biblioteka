import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  user: User;

  constructor(private router: ActivatedRoute, private dataService: DataService,private route: Router) { }

  ngOnInit() {
    this.loadUser();

  }
  loadUser(): void {
    this.router.params.subscribe(
      (params: Params) => {
        let id: number = params["id"];  
        this.dataService.getUserById(id).subscribe(
          (user: User) => {
            this.user = user;
           
          }
        );
      }
    );
  }
}
