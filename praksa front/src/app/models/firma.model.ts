export class Firma
{
    public username: string;
    public password: string;
    public id: number;
    public naziv: string;
    public adresa: string;
    public logo: string;
    public link: string;
    public email: string;
}