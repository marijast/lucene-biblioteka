import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { MaterialModule } from './material.module';
import { UserComponent } from './components/user/user.component';
import { UserInfoComponent } from './components/user-info/user-info.component';
import { AddPostComponent } from './components/add-post/add-post.component';
import { HomeComponent } from './components/home/home.component';
import { PostComponent } from './components/post/post.component';
import { NalogFirmaComponent } from './components/nalog-firma/nalog-firma.component';
import { RegistracijaComponent } from './components/registracija/registracija.component';
import { HeaderComponent } from './components/header/header.component';
import { FirmaComponent } from './components/firma/firma.component';
import { OglasiJedneFirmeComponent } from './components/oglasi-jedne-firme/oglasi-jedne-firme.component';
import { SviPostoviComponent } from './components/svi-postovi/svi-postovi.component';
import { BrisanjePostaComponent } from './components/brisanje-posta/brisanje-posta.component';
import { KompanijeComponent } from './components/kompanije/kompanije.component';
import { JednaFirmaComponent } from './components/jedna-firma/jedna-firma.component';
import { Home1Component } from './components/home1/home1.component';
import { LogovanjeZaPrijavuComponent } from './components/logovanje-za-prijavu/logovanje-za-prijavu.component';
import { ObavestenjaComponent } from './components/obavestenja/obavestenja.component';
import { Obavestenja1Component } from './components/obavestenja1/obavestenja1.component';
import { PostPrikazComponent } from './components/post-prikaz/post-prikaz.component';
import { MatFormFieldModule } from '@angular/material';
//import { AngularFontAwesomeModule } from 'angular-font-awesome/dist/src/angular-font-awesome.module';
//import { AngularFontAwesomeModule } from 'angular-font-awesome';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    UserComponent,
    UserInfoComponent,
    AddPostComponent,
    HomeComponent,
    PostComponent,
    NalogFirmaComponent,
    RegistracijaComponent,
    HeaderComponent,
    FirmaComponent,
    OglasiJedneFirmeComponent,
    SviPostoviComponent,
    BrisanjePostaComponent,
    KompanijeComponent,
    JednaFirmaComponent,
    Home1Component,
    LogovanjeZaPrijavuComponent,
    ObavestenjaComponent,
    Obavestenja1Component,
    PostPrikazComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpClientModule,
    MatFormFieldModule ,
  ],
  entryComponents: [
    RegistracijaComponent,
    LoginComponent,
    FirmaComponent
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
