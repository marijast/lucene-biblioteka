﻿using praksa.Entities;
using System;
using NHibernate;
using praksa.DataWrappers;
using praksa.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Providers
{
    public class UpitProvider
    {
        public bool AddUpit(Upit upit)
        {
            try
            {
                ISession session = DataLayer.GetSession();
                session.Save(upit);


                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }
        public bool AddUpitDW(NoviUpit noviU)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                Upit upit = new Upit();
                upit.Mail = noviU.mail;
                upit.ZadatiUpit = noviU.zadatiUpit;


                session.Save(upit);

           

                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }
     
        public List<Upit> GetUpiti()
        {
            NHibernate.ISession s = DataLayer.GetSession();

            List<Upit> upiti = s.Query<Upit>()
                .Select(p => p).ToList();
            return upiti;
        }

        public int ObrisiUpit(int id)
        {
            try
            {
                ISession s = DataLayer.GetSession();

                Upit upit = s.Query<Upit>().Where(v => v.Id == id).Select(p => p).FirstOrDefault();

                s.Delete(upit);
                s.Flush();
                s.Close();

                return 1;
            }
            catch (Exception exc)
            {
                return -1;
            }

        }

    }
}
