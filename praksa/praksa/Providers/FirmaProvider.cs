﻿using NHibernate;
using praksa.DataWrappers;
using praksa.DTOs;
using praksa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MailKit;
using MimeKit;



namespace praksa.Providers
{
    public class FirmaProvider
    {
        public IEnumerable<praksa.DTOs.FirmaView> GetFirmeView()
        {
            NHibernate.ISession s = DataLayer.GetSession();

            IEnumerable<DTOs.FirmaView> firme = s.Query<Firma>()
                .Select(p => new DTOs.FirmaView(p));
            return firme;
        }
        public List<Firma> GetFirme()
        {
            NHibernate.ISession s = DataLayer.GetSession();

            List<Firma> firme = s.Query<Firma>()
                .Select(p => p).ToList();
            return firme;
        }
        public Firma GetFirma(int id)
        {
            NHibernate.ISession s = DataLayer.GetSession();

            return s.Query<Firma>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();
        }

        public bool AddFirma(NovaFirma novafrima)
        {
            try
            {
                ISession session = DataLayer.GetSession();

                Firma firma = new Firma();
                firma.Naziv = novafrima.Naziv;
                firma.Adresa = novafrima.Adresa;
                firma.Logo = novafrima.Logo;               
                firma.Link = novafrima.Link;
                firma.Password = novafrima.Password;
                firma.Username = novafrima.Username;
                firma.Email = novafrima.Email;


                session.Save(firma);

                /*  foreach (String str in dest.ImaSlike)
                  {
                      Slike sl = new Slike();
                      sl.UrlSlike = str;
                      sl.DestinacijaS = d;

                      s.Save(sl);
                  }*/

                session.Flush();
                session.Close();

                return true;
            }
            catch (Exception ec)
            {
                return false;
            }
        }

      /*  public FirmaView VratiFirmuCijiJePost(int id)
        {
            ISession s = DataLayer.GetSession();

            Post post = s.Query<Post>()
                 .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            FirmaView firma = s.Query<Firma>()
                 .Where(v => v.Id == post.PripadaFirmi.Id);

            return firma;
        }*/

        public FirmaView VratiFirmuCijiJePost(int id)
        {
            ISession s = DataLayer.GetSession();

            Post post = s.Query<Post>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            Firma firma = s.Query<Firma>()
                 .Where(v => v.Id==post.PripadaFirmi.Id ).Select(p => p).FirstOrDefault();

            FirmaView fw = new FirmaView(firma);
            return fw;
           
        }
     
       public Firma VratiFirmuCijiJePostId(int id)
        {
            ISession s = DataLayer.GetSession();

            Post post = s.Query<Post>()
                .Where(v => v.Id == id).Select(p => p).FirstOrDefault();

            Firma firma = s.Query<Firma>()
                 .Where(v => v.Id==post.PripadaFirmi.Id ).Select(p => p).FirstOrDefault();

            
            return firma;
           
        }
        public bool PostojiFirma(String username, String password)
        {
            try
            {
                ISession s = DataLayer.GetSession();
                Firma postoji = s.Query<Firma>()
                 .Where(v => v.Username.Equals(username) && v.Password.Equals(password))
                 .Select(p => p).FirstOrDefault();

                if (postoji != null)
                    return true; // sve tacno

                return false; //nista se ne poklapa
            }
            catch (Exception ec)
            {
                return false;
            }
        }
        public int vratiIdFirme(Firma firma)
        {
            ISession s = DataLayer.GetSession();
            List<Post> sviOglasi = new List<Post>();
            Post jedanOglas = new Post();

            int id = s.Query<Firma>()
                .Where(v => v.Password.Equals(firma.Password) && v.Username.Equals(firma.Username))
                .Select(p => p.Id).FirstOrDefault();

            return id;
        }


        public Firma FirmaByUsername(String username)
        {
            ISession s = DataLayer.GetSession();

            return s.Query<Firma>()
                .Where(v => v.Username == username).Select(p => p).FirstOrDefault();
        }

        
        public void Mail(Firma firma, String poruka)
        {
            var message = new MimeMessage();
            message.From.Add(new MailboxAddress("New application for internship", "internshipservice@gmail.com"));
            message.To.Add(new MailboxAddress(firma.Naziv, firma.Email));
            message.Subject = "Prijava za praksu";

            message.Body = new TextPart("plain")
            {
                Text = "Postovani,\n" + poruka + " \n S poštovanjem, \n Vaš Internship service!"
            };
            using (var client = new SmtpClient())
            {
                client.Connect("smtp.gmail.com", 587, false);
                client.Authenticate("internshipservice", "asdfghhgfdsa10");
                client.Send(message);
                client.Disconnect(true);
            }

        }
    }

}
