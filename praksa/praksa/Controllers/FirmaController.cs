﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using praksa.DataWrappers;
using praksa.DTOs;
using praksa.Entities;
using praksa.Providers;

namespace praksa.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FirmaController : ControllerBase
    {
        [HttpGet]
        public IEnumerable<DTOs.FirmaView> Get()
        {

            FirmaProvider provider = new FirmaProvider();
            IEnumerable<DTOs.FirmaView> firme = provider.GetFirmeView();

            return firme;
        }
        /*   public IEnumerable<Firma> Get()
            {

                FirmaProvider provider = new FirmaProvider();
                IEnumerable<Firma> Firmas = provider.GetFirmas();

                return Firmas;
            }*/


        // GET api/<controller>/5
        [HttpGet("{id}")]
        public Firma Get(int id)
        {
            FirmaProvider provider = new FirmaProvider();
            Firma Firma = provider.GetFirma(id);

            return Firma;
        }

        [HttpPost]
        [Route("addFirma")]
        public IActionResult Firma([FromBody] NovaFirma firma)
        {
            FirmaProvider provider = new FirmaProvider();
            UserProvider userP = new UserProvider();
            if (provider.PostojiFirma(firma.Username, firma.Password) || userP.Postoji(firma.Username,firma.Password))
            {
                var tip = new { tip = "Postoji" };
                return Ok(tip);
            }
            bool po = provider.AddFirma(firma);
            if (po)
            {
                var tip = new { tip = "dodato" };
                return Ok(tip);
            }
            return NotFound();
        }

        [HttpGet("ucitaj/{idPosta}")]
        public FirmaView VratiFirmuCijiJePost(int idPosta)
        {
            FirmaProvider provider = new FirmaProvider();
            FirmaView firma = provider.VratiFirmuCijiJePost(idPosta);
            if (firma == null)
                return null;

            return firma;
        }

        [HttpGet("getFirmaByUsername/{username}")]
        public FirmaView VratiFirmuUsername(string username)
        {
            FirmaProvider provider = new FirmaProvider();
            Firma f = provider.FirmaByUsername(username);
            FirmaView firma = new FirmaView(f);
            if (firma == null)
                return null;

            return firma;
        }

        [HttpPost]
        [Route("vratiSveOglase")]
        public List<Post> vratiSveOglase([FromBody] Firma firma)
        {
            FirmaProvider provider = new FirmaProvider();
            List<Post> JsonObj = new List<Post>();
            PostProvider postProvider = new PostProvider();
            if (provider.PostojiFirma(firma.Username, firma.Password))
            {
                JsonObj = postProvider.vratiSveOglase(firma);
            }

            return JsonObj;
        }
    
        [HttpPost]
        [Route("vratiId")]
        public int vratiIdFirme([FromBody] Firma firma)
        {
            FirmaProvider provider = new FirmaProvider();
            int id;
            
            if (provider.PostojiFirma(firma.Username, firma.Password))
            {
                id = provider.vratiIdFirme(firma);
            }

            return 0;
        }

        [HttpGet]
        [Route("oglasiFirme/{username}")]
        public List<Post> vratiOglaseFirme(string username)
        {
            FirmaProvider firmaProvider= new FirmaProvider();
            List<Post> posts = new List<Post>();
            PostProvider postProvider = new PostProvider();

            Firma firma = firmaProvider.FirmaByUsername(username);

            posts = postProvider.vratiSveOglase(firma);

            return posts;
            
        }
    }
}
