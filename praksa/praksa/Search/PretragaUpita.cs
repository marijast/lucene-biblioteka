﻿using praksa.Entities;
using praksa.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Search
{
    public class PretragaUpita
    {

        public void PretraziUpite()
        {
            UpitProvider provider = new UpitProvider();
            UserProvider userProvider = new UserProvider();
            List<Upit> upiti = new List<Upit>();
            LuceneSearch pretraga = new LuceneSearch();

            upiti = provider.GetUpiti();
            foreach(Upit u in upiti)
            {
                List<Post> postovi = new List<Post>();
                postovi=pretraga.SearchOnIndex(u.ZadatiUpit, "Opis");
                if(postovi.Count!=0)
                {
                    foreach(Post p in postovi)
                    {
                        string poruka = " Vas upit: \n" + u.ZadatiUpit + ", \n  Post: \n"
                     +  p.Opis + ", \n  trajanje:" + p.Trajanje + ", \n grad: " + p.Grad + "  \n kompanija:"
                    + p.PripadaFirmi.Naziv + " . \n Posetite sajt kompanije: " + p.PripadaFirmi.Link;
                        userProvider.MailPretraga(u.Mail, poruka);
                    }
                 provider.ObrisiUpit(u.Id);
                }
                List<Post> postoviMulti = new List<Post>();
                string[] fields = { "Grad", "Trajanje" };
                postoviMulti = pretraga.SearchOnIndexMulti(u.ZadatiUpit,fields);
                if (postoviMulti.Count != 0)
                {
                    foreach (Post p in postoviMulti)
                    {
                        string poruka = "Trazili ste praksu sa sledecim kriterijumima : \n" + u.ZadatiUpit + ", \n  Post: \n"
                     + p.Opis + ", \n  trajanje:" + p.Trajanje + ", \n grad: " + p.Grad + "  \n kompanija:"
                    + p.PripadaFirmi.Naziv + " . \n Posetite sajt kompanije: " + p.PripadaFirmi.Link;
                        userProvider.MailPretraga(u.Mail, poruka);
                    }
                    provider.ObrisiUpit(u.Id);
                }



            }
        }
    }
}
