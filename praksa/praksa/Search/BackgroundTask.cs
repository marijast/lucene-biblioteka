﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace praksa.Search
{
    public class BackgroundTask : IHostedService, IDisposable
    {
        private int number = 0;
        private readonly ILogger<BackgroundTask> logger;
        private Timer timer;
        private PretragaUpita pu = new PretragaUpita();
   

        public BackgroundTask(ILogger<BackgroundTask> logger)
        {
            this.logger = logger;
        }


        public void Dispose()
        {
            timer?.Dispose();
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
          
            timer = new Timer(o =>
            {
                // Interlocked.Increment(ref number);       
                //  logger.LogInformation($"Printing from worker number: {number}");
                pu.PretraziUpite();

            }
                , null,
                TimeSpan.Zero,//odmah pocinje
            //    TimeSpan.FromSeconds(21600)//1800)//period
                TimeSpan.FromSeconds(900)//1800)//period
                );
            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {

            logger.LogInformation("Printing worker stopping");
            return Task.CompletedTask;
        }
    }
}
