﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using praksa.Entities;

namespace praksa.Mapping
{
    public class UserMapping : ClassMap<User>
    {
        public UserMapping()
        {
            Table("user");

            Id(x => x.Id, "Id").GeneratedBy.Increment();

            Map(x => x.Ime, "Ime");
            Map(x => x.Prezime, "Prezime");
            Map(x => x.Username, "Username");
            Map(x => x.Password, "Password");
            Map(x => x.Email, "Email");

          HasManyToMany(x => x.PrijavljenZaOglase)
                .Table("prijavljivanjepraksa")
                .ParentKeyColumn("userId")
                .ChildKeyColumn("oglasId")
                .Cascade.All();
          // HasMany(x => x.PrijavljenZaOglase).KeyColumn("userId").LazyLoad().Cascade.All().Inverse();

        }
    }
}
