﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentNHibernate.Mapping;
using praksa.Entities;


namespace praksa.Mapping
{
    public class UpitMapping : ClassMap<Upit>
    {
        public UpitMapping()
        {
            Table("upit");

            Id(x => x.Id, "Id").GeneratedBy.Increment();

            Map(x => x.Mail, "Mail");
            Map(x => x.ZadatiUpit, "ZadatiUpit");

        }
    }
}
