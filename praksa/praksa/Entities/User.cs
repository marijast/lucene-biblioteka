﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.Entities
{
    public class User
    {
        public virtual int Id { get; protected set; }
        public virtual string Ime { get; set; }
        public virtual string Prezime { get; set; }
        public virtual string Username { get; set; }
        public virtual string Password { get; set; }
        public virtual string Email { get; set; }
        public virtual IList<Post> PrijavljenZaOglase { get; set; }

        public User()
        {
            PrijavljenZaOglase = new List<Post>();
        }

    }
}
