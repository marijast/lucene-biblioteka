﻿using praksa.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DataWrappers
{
    public class AddPost
    {
        public String Naziv { get; set; }
        public String Opis { get; set; }
        public String Trajanje { get; set; }
        public String Grad { get; set; }
        public Firma Firma { get; set; }


    }
}
