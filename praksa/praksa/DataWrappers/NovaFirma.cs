﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace praksa.DataWrappers
{
    public class NovaFirma
    {
        public String Naziv { get; set; }
        public String Adresa { get; set; }
        public String Logo { get; set; }
        public String Link { get; set; }
        public String Username { get; set; }
        public String Password { get; set; }
        public String Email { get; set; }
    }
}
